# Adjust Purchase Tracking

## Integration Steps

1) In order to integrate Adjust tracking, please make sure you have completed all the steps required for <a href="3.2_MMP_Adjust.html" target="_blank">Adjust integration</a>.

2) **"Install"** or **"Upload"** FG AdjustPurchaseTracker plugin from the FunGames Integration Manager in Unity, or download it from here.

3) Click on the **"Prefabs and Settings"** button from FunGames Integration window to fill up your scene with required components and create the Settings asset.

4) Once done, you will have to bind your existing FG Products (in _Resources/FunGames/IAP_) with their corresponding Adjust token in **FGAdjustPurchaseTrackerSettings**.

![](_source/iap_AdjustTrackingSettings1.png)